package com.ia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {
	public static void main(String[] args) {
		List<String> fr = new ArrayList<>();
		Map<String, String> d = new HashMap<>();

		fr.add("7@comp1.COM|4|11|GDSPV");
		fr.add("7@comp1.COM|16|82|GDSPV");
		fr.add("13@comp1.COM|21|82|GDSPV");
		fr.add("26@comp1.COM|19|82|GDSPV");

		d.put("7@comp1.COM", "199");
		d.put("8@comp4.COM", "200");
		d.put("13@comp1.COM", "205");
		test(fr, d);

	}

	static void test(List<String> fr, Map<String, String> d) {
		List<String> resultList = new ArrayList<>();

		for (int i = 0; i < fr.size(); i++) {
			String emailRest = getValidEmailFromStringAndTheRest(fr.get(i));
			String emailRestArray[] = emailRest.split(" ");
			String email = emailRestArray[0];
			String rest = emailRestArray[1];
			System.err.println(email);
			if (foundEmailInMap(email, d)) {
				resultList.add(d.get(email) + rest);
			} else {
				d.put(email, String.valueOf(getMaxValueFromValueMap(d)));
				resultList.add(d.get(email) + rest);
			}
		}
		System.out.println(resultList);
		System.out.println(d);
	}

	static boolean foundEmailInMap(String email, Map<String, String> d) {
		for (String value : d.keySet()) {
			if (value.equals(email)) {
				return true;
			}
		}
		return false;
	}

	static int getMaxValueFromValueMap(Map<String, String> map) {
		int max = 0;
		for (String value : map.values()) {
			if (Integer.parseInt(value) > max) {
				max = Integer.parseInt(value);
			}
		}
		return max + 1;
	}

	static String getValidEmailFromStringAndTheRest(String value) {
		StringBuilder email = new StringBuilder();
		int i = 0;
		while (!isValidEmail(email.toString())) {
			email.append(value.charAt(i));
			i++;
		}
		String rest = value.substring(i, value.length());
		return email.toString() + " " + rest;
	}

	static boolean isValidEmail(String email) {
		String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]+[\\w]$";
		return email.matches(regex);
	}
}
